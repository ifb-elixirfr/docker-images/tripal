FROM quay.io/galaxy-genome-annotation/tripal:v2.x

RUN apt-get update && apt-get install  ca-certificates openssl libgnutls30
